RADJOU Pravine
TP2 - MAIN0501

Commandes pour controller la grue :

Pour déplacer la flèche : utilisez Gauche/Droite
Pour déplacer le chariot : utilisez Haut/Bas
Pour déplacer la moufle : utilisez Shift/Ctrl
Pour lacher un objet accroché : utilisez Espace
Pour changer de caméra : utilisez C