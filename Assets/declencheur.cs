using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declencheur : MonoBehaviour
{
    private int cubeCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cube"))
        {
            cubeCount += 1;
            Debug.Log("Nombre de cubes dans la zone : " + cubeCount);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Cube"))
        {
            cubeCount -= 1;
            Debug.Log("Nombre de cubes dans la zone : " + cubeCount);
        }
    }
}