using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public Camera mainCamera;
    public Camera cabineCamera;
    public Camera chariotCamera;

    private Camera activeCamera;

    // Start is called before the first frame update
    void Start()
    {
        // Caméra par défaut
        activeCamera = mainCamera;
        mainCamera.gameObject.SetActive(true);
        cabineCamera.gameObject.SetActive(false);
        chariotCamera.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            SwitchCamera();
        }
    }

    void SwitchCamera()
    {
        if (activeCamera == mainCamera)
        {
            activeCamera = cabineCamera;
            mainCamera.gameObject.SetActive(false);
            cabineCamera.gameObject.SetActive(true);
        }
        else if (activeCamera == cabineCamera)
        {
            activeCamera = chariotCamera;
            cabineCamera.gameObject.SetActive(false);
            chariotCamera.gameObject.SetActive(true);
        }
        else
        {
            activeCamera = mainCamera;
            chariotCamera.gameObject.SetActive(false);
            mainCamera.gameObject.SetActive(true);
        }
    }
}